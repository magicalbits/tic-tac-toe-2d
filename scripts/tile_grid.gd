class_name TileGrid

extends CanvasGroup

signal clicked(source)
signal player_won(player)

var tiles: Array[Node2D]
var grid: Array[Array]
var placed_tiles = 0

const GRID_EDGE_SIZE = 3

enum TileState {
	EMPTY,
	O,
	X
}


# Called when the node enters the scene tree for the first time.
func _ready():
	# Initialize tiles array
	for i in range(1, 10):
		tiles.append(get_node("Tile" + str(i)))
	
	# Connect each tile's 'clicked' signal to the given method
	for tile in tiles:
		tile.clicked.connect(_on_tile_clicked)
	
	clear_grid()


func _on_tile_clicked(source: String):
	print(name + ": tile '" + source + "' clicked")
	clicked.emit(source)


func clear_grid():
	placed_tiles = 0
	grid.clear()
	for i in range(GRID_EDGE_SIZE):
		var arr = Array()
		for j in range(GRID_EDGE_SIZE):
			arr.append(TileState.EMPTY)
		grid.append(arr)


func is_tile_empty(idx: int):
	return grid[idx / GRID_EDGE_SIZE][idx % GRID_EDGE_SIZE] == TileState.EMPTY


func place_tile(tile_state: TileState, idx: int):
	if is_tile_empty(idx):
		grid[idx / GRID_EDGE_SIZE][idx % GRID_EDGE_SIZE] = tile_state
		placed_tiles += 1
	else:
		print(name + ": tile not empty => not emitted")
	
	# Check for winning rows
	if placed_tiles >= GRID_EDGE_SIZE * 2 - 1:
		if not _is_winner() and placed_tiles == GRID_EDGE_SIZE * GRID_EDGE_SIZE:
			player_won.emit("tie")


func _is_winner():
	for line in grid:
		# Horizontal row checks
		if _is_winning_row(line):
			return true
	
	# Invert matrix (grid)
	var vertical_lines: Array[Array]
	for i in range(GRID_EDGE_SIZE):
		for j in range(GRID_EDGE_SIZE):
			if vertical_lines.size() < GRID_EDGE_SIZE:
				vertical_lines.append([grid[i][j]])
			else:
				vertical_lines[j].append(grid[i][j])
	
	# Vertical row checks
	for line in vertical_lines:
		if _is_winning_row(line):
			return true
	
	# Diagonal row checks
	var d1: Array[TileState]
	var d2: Array[TileState]
	# Primary diagonal
	for i in range(GRID_EDGE_SIZE):
		d1.append(grid[i][i])
	# Secondary diagonal
	for i in range(GRID_EDGE_SIZE):
		d2.append(grid[GRID_EDGE_SIZE - i - 1][i])
	for line in [d1, d2]:
		if _is_winning_row(line):
			return true


func _is_winning_row(line: Array):
	if not TileState.EMPTY in line:
		if not TileState.X in line:
			# O wins
			player_won.emit("O")
			return true
		elif not TileState.O in line:
			# X wins
			player_won.emit("X")
			return true
	return false
