extends CanvasLayer

signal start_game


func _ready():
	$WinnerLabel.hide()


func _on_start_button_pressed():
	$StartButton.hide()
	$WinnerLabel.hide()
	start_game.emit()
