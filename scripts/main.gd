extends Node2D

@export var tile_o_scene: PackedScene
@export var tile_x_scene: PackedScene
var tile_grid: TileGrid
var current_player = Player.X
var placed_tiles: Array[Sprite2D]

enum Player {
	O,
	X
}


func _ready():
	tile_grid = get_node("TileGrid")


func _start_game():
	current_player = Player.X
	
	# Connect grid's 'clicked' signal
	tile_grid.clicked.connect(_place_tile)
	
	# Delete any placed tiles from previous games
	for tile in placed_tiles:
		tile.queue_free()
	placed_tiles.clear()
	
	# Clear grid state
	tile_grid.clear_grid()
	
	$HUD/GameTitleLabel.hide()
	$HUD/WinnerLabel.hide()


func _place_tile(source: String):
	var tile: Sprite2D = tile_grid.get_node(source)
	
	# Extracts the index from the tile name
	var idx = int(source.substr(source.length() - 1)) - 1
	
	# Don't place a tile if the tile is already occupied
	if not tile_grid.is_tile_empty(idx):
		print(name + ": tile not empty => not emitted")
		return
	
	if current_player == Player.X:
		var x: Sprite2D = tile_x_scene.instantiate()
		x.global_position = tile.global_position
		add_child(x)
		placed_tiles.append(x)
		tile_grid.place_tile(TileGrid.TileState.X, idx)
		current_player = Player.O
	else:
		var o: Sprite2D = tile_o_scene.instantiate()
		o.global_position = tile.global_position
		add_child(o)
		placed_tiles.append(o)
		tile_grid.place_tile(TileGrid.TileState.O, idx)
		current_player = Player.X


func _on_tile_grid_player_won(player: String):
	if player == "tie":
		$HUD/WinnerLabel.text = "Tie"
	else:
		$HUD/WinnerLabel.text = "Player %s won" % player
	$HUD/WinnerLabel.show()
	$HUD/StartButton.show()
	
	# Disables further grid modifications
	tile_grid.clicked.disconnect(_place_tile)
